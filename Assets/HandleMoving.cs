﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleMoving : MonoBehaviour
{

    MovingAnimation moving;

    // Start is called before the first frame update
    void Start()
    {
        moving = GameObject.Find("Hanoi").gameObject.GetComponent<MovingAnimation>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Move(GameObject from, GameObject to)
    {
        TrackGameState.linkRingListAndATowers.Find(i => i.realTower == to).realListOfRings.Insert(0, TrackGameState.linkRingListAndATowers.Find(i => i.realTower == from).realListOfRings[0]);
        GameObject moveableObject = TrackGameState.linkRingListAndATowers.Find(i => i.realTower == from).realListOfRings[0].realRing;
        moveableObject.transform.parent = to.transform;
        TrackGameState.linkRingListAndATowers.Find(i => i.realTower == from).realListOfRings.RemoveAt(0);
        moving.MoveTo(moveableObject, MoveToCorrectPos(to), true);
        Debug.Log("move");
    }

    private Vector3 MoveToCorrectPos(GameObject tower)
    {
        GameObject platform = GameObject.Find("Hanoi").gameObject;
        float platHeight = platform.GetComponent<Renderer>().bounds.size.y;
        float previousSize = 0;
        int i;
        for (i = 0; i < TrackGameState.linkRingListAndATowers.Find(s => s.realTower == tower).realListOfRings.Count - 1; i++)
        {
            previousSize += TrackGameState.linkRingListAndATowers.Find(s => s.realTower == tower).realListOfRings[i].realRing.GetComponent<Renderer>().bounds.size.y;
        }
        float sizeOfTheNewRing = TrackGameState.linkRingListAndATowers.Find(s => s.realTower == tower).realListOfRings[i].realRing.GetComponent<Renderer>().bounds.size.y;

        float correctHeight = platform.transform.position.y + platHeight / 2 + previousSize + sizeOfTheNewRing / 2;

        Vector3 correctPos = new Vector3(tower.transform.position.x, correctHeight, tower.transform.position.z);

        return correctPos;
    }
}
