﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingAnimation : MonoBehaviour
{
    public float heightAboveTowers = 0.12f;
    List<MovingGameObjects> currentlyMoving = new List<MovingGameObjects>(); // used to stop coroutine if another one comes with conflicting orders


    public void MoveTo(GameObject objectToMove, Vector3 Position, bool useAnimation)
    {
        if (useAnimation)
        {
            if (currentlyMoving.Exists(i => i.ObjectBeingAnimated == objectToMove))
            {
                StopCoroutine(currentlyMoving.Find(i => i.ObjectBeingAnimated == objectToMove).Animation);
                currentlyMoving.Remove(currentlyMoving.Find(i => i.ObjectBeingAnimated == objectToMove));
            }
            IEnumerator anim = MoveToWithAnimation(objectToMove, Position);
            currentlyMoving.Add(new MovingGameObjects(objectToMove, anim));
            StartCoroutine(anim);
        }
        else
        {
            objectToMove.transform.position = Position;
        }
    }

    public IEnumerator MoveToWithAnimation(GameObject objectToMove, Vector3 Position)
    {
        bool[] sequences = { false, false, false };
        GameObject tower = GameObject.Find("Hanoi").transform.GetChild(0).gameObject;
        float towerHeight = tower.GetComponent<Renderer>().bounds.size.y;

        Vector3 firstTarget = new Vector3(objectToMove.transform.position.x, tower.transform.position.y + towerHeight/2 + heightAboveTowers, objectToMove.transform.position.z);
        Vector3 secondTarget = new Vector3(Position.x, firstTarget.y, Position.z);
        Vector3 thirdTarget = Position;
        Debug.Log(firstTarget.y + " Y");

        while (objectToMove.transform.position != Position)
        {

            if (!sequences[0])
            {
                Vector3 direction = new Vector3(0, objectToMove.transform.position.y + 0.01f, 0);
                objectToMove.transform.Translate(Vector3.up*0.01f, Space.Self);

                //objectToMove.transform.position = new Vector3(objectToMove.transform.position.x, objectToMove.transform.position.y + 0.01f, objectToMove.transform.position.z);

                if (objectToMove.transform.position.y >= firstTarget.y )
                {
                    sequences[0] = true;
                    objectToMove.transform.position = firstTarget;
                }
            }
            else if (!sequences[1])
            {
                //dest-current
                // Vector3 direction = secondTarget - objectToMove.transform.position;
                Vector3 direction = new Vector3(secondTarget.x - objectToMove.transform.position.x, 0, secondTarget.z - objectToMove.transform.position.z);
                Vector3.Normalize(direction);
                direction = direction / 10;
                //objectToMove.transform.position = new Vector3(objectToMove.transform.position.x - direction.x, firstTarget.y, objectToMove.transform.position.z - direction.z);
                objectToMove.transform.Translate(direction, Space.Self);
                if (Mathf.Abs(objectToMove.transform.position.z-secondTarget.z) < 0.01f && Mathf.Abs(objectToMove.transform.position.x - secondTarget.x) < 0.01f)
                {
                    Debug.Log("dawpoidawopdkij2");

                    sequences[1] = true;
                    objectToMove.transform.position = secondTarget;
                }
            }
            else if (!sequences[2])
            {
              //  objectToMove.transform.position = new Vector3(objectToMove.transform.position.x, objectToMove.transform.position.y - 0.01f, objectToMove.transform.position.z);
                objectToMove.transform.Translate(Vector3.down * 0.01f, Space.Self);

                if (objectToMove.transform.position.y <= thirdTarget.y)
                {
                    sequences[2] = true;
                    objectToMove.transform.position = thirdTarget;

                }
            }
            yield return new WaitForSeconds(0.01f);
        }
        currentlyMoving.Remove(currentlyMoving.Find(i => i.ObjectBeingAnimated == objectToMove));

    }
}
public class MovingGameObjects
{
    public GameObject ObjectBeingAnimated;
    public IEnumerator Animation;

    public MovingGameObjects(GameObject objectBeingAnimated, IEnumerator animation)
    {
        ObjectBeingAnimated = objectBeingAnimated;
        Animation = animation;
    }

}