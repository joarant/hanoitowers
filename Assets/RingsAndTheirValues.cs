﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RingsAndTheirValues : IComparable<RingsAndTheirValues>
{
    public int realValue;
    public GameObject realRing;

    public RingsAndTheirValues(int value, GameObject ring)
    {
        realValue = value;
        realRing = ring;
    }


    public int CompareTo(RingsAndTheirValues other)
    {
        if (other == null)
        {
            return 1;
        }

        //Return the difference in power.
        return realValue - other.realValue;
    }

    
}

public class LinkRingListAndATower
{
    public List<RingsAndTheirValues> realListOfRings;
    public GameObject realTower;

    public LinkRingListAndATower(List<RingsAndTheirValues> listOfRings, GameObject tower)
    {
        realListOfRings = listOfRings;
        realTower = tower;
    }

}