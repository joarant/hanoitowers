﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selecting : MonoBehaviour
{
    public static List<GameObject> selected = new List<GameObject>();
    HandleMoving handleMoving;
    private Color ogColor;

    // Start is called before the first frame update
    void Start()
    {
        handleMoving = GetComponent<HandleMoving>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit RayHit;
            if (Physics.Raycast(ray, out RayHit))
            {
                GameObject ObjectHit = RayHit.transform.gameObject;
                Vector3 Hitpoint = RayHit.point;
                if (ObjectHit != null)
                    Debug.DrawLine(Camera.main.transform.position, Hitpoint, Color.blue, 0.5f);

                if (ObjectHit.tag == "Tower")
                {
                    Debug.Log(ObjectHit.name);
                    Debug.Log(selected.Count);

                    if (selected.Count == 1)
                    {
                        if (selected[0] != ObjectHit)
                        {
                            if (TrackGameState.linkRingListAndATowers.Find(i => i.realTower == ObjectHit).realListOfRings.Count == 0)
                            {
                                selected.Add(ObjectHit);
                                handleMoving.Move(selected[0], selected[1]);
                                ResetColor();
                                selected.Clear();
                            }
                            else if (TrackGameState.linkRingListAndATowers.Find(i => i.realTower == ObjectHit).realListOfRings[0].realValue >
                                TrackGameState.linkRingListAndATowers.Find(i => i.realTower == selected[0]).realListOfRings[0].realValue)
                            {
                                selected.Add(ObjectHit);
                                handleMoving.Move(selected[0], selected[1]);
                                ResetColor();
                                selected.Clear();
                            }

                        }
                        else
                        {
                            ResetColor();
                            selected.Clear();
                        }
                    }
                    else if (TrackGameState.linkRingListAndATowers.Find(i => i.realTower == ObjectHit).realListOfRings.Count > 0 && selected.Count == 0)
                    {
                        selected.Add(ObjectHit);
                        ogColor = ObjectHit.GetComponent<Renderer>().material.color;
                        ObjectHit.GetComponent<Renderer>().material.color = Color.green;
                    }
                    else
                    {
                        selected.Clear();

                    }

                }


            }
            else
            {
                ResetColor();
                selected.Clear();
            }
            //Cast Ray from Camera through Mouse Ends
        }
    }

    void ResetColor()
    {
        for (int i = 0; i < selected.Count; i++)
        {
            selected[0].GetComponent<Renderer>().material.color = ogColor;
        }
    }
}
