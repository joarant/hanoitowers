﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class TrackGameState : MonoBehaviour
{
    List<RingsAndTheirValues> tower1 = new List<RingsAndTheirValues>();
    List<RingsAndTheirValues> tower2 = new List<RingsAndTheirValues>();
    List<RingsAndTheirValues> tower3 = new List<RingsAndTheirValues>();
    public static List<LinkRingListAndATower> linkRingListAndATowers = new List<LinkRingListAndATower>();

    public GameObject[] towers;
    public static GameObject gameWinningTower;

    // Start is called before the first frame update
    void Start()
    {

        linkRingListAndATowers.Add(new LinkRingListAndATower(tower1, towers[0]));
        linkRingListAndATowers.Add(new LinkRingListAndATower(tower2, towers[1]));
        linkRingListAndATowers.Add(new LinkRingListAndATower(tower3, towers[2]));


        Initialize ini = GetComponent<Initialize>();
        ini.Create();

    }

    public static void CheckGameState()
    {
        
        if (linkRingListAndATowers.Find(i => i.realTower == gameWinningTower).realListOfRings.Count == 3)
        {
            Debug.Log("you won");
        }

    }



}
