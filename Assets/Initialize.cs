﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialize : MonoBehaviour
{
    public int numberOfRings = 3;
    public GameObject startTower;
    public GameObject Ring;
    public float sizeDifference = 0.05f;
    public Color[] colorRotation;
    private int counter = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Create()
    {
        float platHeight = GetComponent<Renderer>().bounds.size.y;
        float previousSize = 0;

        for (int i = 1; i <= numberOfRings; i++)
        {
            GameObject ringCopy = Instantiate(Ring);
            RingValue ringValue = ringCopy.GetComponent<RingValue>();
            ringCopy.transform.localScale = new Vector3(ringCopy.transform.localScale.x - sizeDifference * i, ringCopy.transform.localScale.y, ringCopy.transform.localScale.z - sizeDifference * i);
            ringCopy.transform.parent = startTower.transform;
            ringValue.value = numberOfRings - i;
            AssignColor(ringCopy);
            TrackGameState.linkRingListAndATowers.Find(s => s.realTower == startTower).realListOfRings.Add(new RingsAndTheirValues(ringValue.value, ringCopy));
            float sizeOfTheNewRing = ringCopy.GetComponent<Renderer>().bounds.size.y;
            float correctHeight = transform.position.y + platHeight / 2 + previousSize + sizeOfTheNewRing / 2;
            Vector3 correctPos = new Vector3(startTower.transform.position.x, correctHeight, startTower.transform.position.z);
            ringCopy.transform.position = correctPos;
            previousSize += sizeOfTheNewRing;
        }
        TrackGameState.linkRingListAndATowers.Find(s => s.realTower == startTower).realListOfRings.Sort();
        //TrackGameState.linkRingListAndATowers.Find(s => s.realTower == startTower).realListOfRings.Reverse();
    }

    private void AssignColor(GameObject ring)
    {
        ring.GetComponent<Renderer>().material.color = colorRotation[counter];
        counter++;
        if (counter == colorRotation.Length)
        {
            counter = 0;
        }
    }
}
